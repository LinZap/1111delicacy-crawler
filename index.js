const co  = require('./lib/co'),
	  parseList = require('./src/parseList'),
	  parsePage = require('./src/parsePage'),
	  low = require('lowdb'),
	  db = low('data.json')

db.defaults({
	data: [],
	error: [],
	page: 1
}).value()


co(function*(){

	let page = db.get('page').value()

	while (page) {
		
		console.log('fetch page:',page)
		let rests = yield* parseList(page)

		if(!rests){
			console.log('process part of error data')
			let one_error = db.get('error[0]').value()
			if(!Array.isArray(one_error) && one_error) {
				db.get('error').remove(one_error).value()
				rests = [one_error]
			}
			else {
				console.log('---- All tasks were finished ----')
				return 
			}
		}

		for (let i=0; i < rests.length; i++) {
			let data = rests[i],
				href = data.href,
				detail = yield* parsePage(href)

			data = Object.assign({},data,detail)
			db.get(detail?'data':'error').push(data).value()
			console.log(detail?'success':'error',data.title)
		}

		db.set('page',++page).value()
	}
})

