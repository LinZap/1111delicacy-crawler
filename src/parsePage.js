const url = require('url'),
	  req = require('../lib/req'),
	  cheerio = require('cheerio'),
	  delay = require('../lib/delay')

/*
	解析外部餐廳列表
*/
module.exports = function*(url) {

	let body

	while(!(body = yield req({ url })))
		yield delay(3)
	
	let $ = cheerio.load(body),
		areas = $('.article'),
		tags = $('.tag_block>a'),
		js = $('script[language="javascript"]')[1],
		foods = $('.imgOver>p')


	if(!areas.length) return error('this page is error:',url)

	let ch =  areas.children(),
		data = {}

	for (let i=0; i<ch.length; i++) {
		let name = ch[i].name,
			el = $(ch[i]),
			txt = el.text().split('│'),
			meta = txt[0].trim(),
			value = txt[1].trim()

		if(name=='div'){	
			data[meta] = getElementsText(el.find('.bgcolor,td'),$)
		}
		else{
			data[meta] = value
		}
	}

	data['tag'] = getElementsText(tags,$)
	data['food'] = getElementsText(foods,$)
	data['location'] = getLoation(js,$)


	return data
}

function getElementsText(el,$) {
	let data = []
	for (let i=0; i < el.length; i++) {
		let txt = $(el[i]).text().trim()
		if(data.indexOf(txt)<0) data.push(txt)
	}
	return data
}

function getLoation(el,$) {
	let locRegExp = new RegExp(/LatLng\((.+),(.+)\);/),
		txt = $(el).text(),
		pat = locRegExp.exec(txt),
		data = pat? {} : null
	if(pat){
		data['lat'] = pat[1].trim()
		data['lng'] = pat[2].trim()
	}
	return data
}


function error(msg) {
	console.log(msg)
	return false
}
