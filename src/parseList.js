const url = require('url'),
	  req = require('../lib/req'),
	  cheerio = require('cheerio'),
	  delay = require('../lib/delay')

const LIST_URL = (page)=> 
		`https://www.1111.com.tw/15sp/delicacy/shoplistAdd.asp?page=${page}&keys=&otype=&otype2=&c0=&d0=&t0=&radioCheck=&st=`

/*
	解析外部餐廳列表
*/
module.exports = function*(page) {

	let body = true

	while(!(body = yield req({url: LIST_URL(page)})))
		yield delay(3)
	

	let $ = cheerio.load(body),
		areas = $('.txtArea'),
		data = []

	if(!areas.length) return error('list in the end!!')


	for (let i=0 ; i < areas.length; i++) {

		let tit = $(areas[i]).find('a[href]'),
			p = $(areas[i]).find('.txt')
		data.push({
			title: tit.text().trim(),
			href: url.resolve(LIST_URL(), tit.attr('href')),
			des: p.text().trim(),
		})

	}

	return data
	
}


function error(msg) {
	console.log(msg)
	return false
}
