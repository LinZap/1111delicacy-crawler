/*
	yieldable delay
*/
module.exports = sec=>fn=>setTimeout(()=>fn(null,sec),sec*1000)