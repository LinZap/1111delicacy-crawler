const pg = require('pg');

module.exports = async function(sql_str,params){

	return new Promise((res,rej)=>{

		var sql = (typeof(sql_str)=='object')? sql_str.sql : sql_str,
			conf = {
				"host": "",
			    "user": "",
			    "password": "", 
			    "database": "", 
			    "port": 0
			},
			client = new pg.Client(conf)

		client.connect((err)=>{
			if(err) rej(false)
			else
				client.query(sql,params,function (err, result) {
					client.end(function(err) {})
					if(err){
						console.log(err,sql,params)
						rej(false)
					}
					else{
						res(result.rows)
					}
				})
		})

	})
}
