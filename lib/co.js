module.exports = function(fn){
	var gen = fn()
	function next(err,data){
		if(err) console.log(err)
		var res = gen.next(data)
		if(!res.done) res.value(next)
	}
	next()
}