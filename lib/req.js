const request = require('request')
/*
	option
	{
		url: '',
		method: 'GET',
		header: { },
		qs:{ },
		json: true,
		postData: {
			mimeType: 'application/x-www-form-urlencoded',
			params: [
				{
					name: 'foo',
					value: 'bar'
				},
				{
					name: 'hello',
					value: 'world'
				}
			]
		}
	}
*/

module.exports = function(option) {

	let def_opt = {
		method: 'GET',
	}

	return function(fn){
		let new_option = Object.assign({},def_opt,option)
		request(new_option,function(error, new_option, body){
			//if(error)  console.error('request error',error)
			fn(error,body)		
		})	
	}
}