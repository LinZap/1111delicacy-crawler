# 1111delicacy Crawler

這是一個抓取 [1111台灣美食](https://www.1111.com.tw/15sp/delicacy/shoplist.asp) 的網路爬蟲，資料會以 JSON 的格式儲存於專案資料夾下的 `data.json`


## Installation

```
git clone https://gitlab.com/LinZap/1111delicacy-crawler.git
cd 1111delicacy-crawler
npm install
```

## Usage

這隻程式會從[這個連結](https://www.1111.com.tw/15sp/delicacy/shoplist.asp)作為起點，將所有店家資料全部抓取。

```
node index.js
```

## Metadata

資料儲存格式如下說明：  

以下為必有：   
* `title` : 店家名稱
* `href` : 原始抓取的網址
* `des` : 店家簡介
* `tag` : 美食標籤
* `food` : 產品簡介
* `location` : 店家經緯度

其餘資料為動態，也就是不保證每筆資料都存在  

* `地址` : 店家地址
* `電話` : 店家電話
* `資料來源` : 資料來源
* `提供服務` : 店家提供服務
* `官方粉絲團` : 店家官方粉絲團
* `營業時間` : 店家營業時間
* `平均消費` : 店家平均消費
* `公休日` : 店家公休日
  
其餘未列出的資料，均為爬蟲自動解析，您可以視情況斟酌取用  


## LICENSE

MIT - ZapLin